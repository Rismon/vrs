<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome2');
});

Auth::routes();
Route::group(['middleware' => ['role:SUADMIN']], function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/admin/dashboard', [App\Http\Controllers\HomeController::class, 'adminDashboard'])->name('admin.dashboard');
    Route::get('/log', [App\Http\Controllers\LogController::class, 'index'])->name('admin.log');
    Route::get('/car', [App\Http\Controllers\CarController::class, 'index'])->name('admin.car');
    Route::get('/user', [App\Http\Controllers\UserController::class, 'index'])->name('admin.user');

    //CAR FEATURE
    Route::get('api/car/fetch/{row}', [App\Http\Controllers\CarController::class, 'fetchCars']);
    Route::post('api/car/create', [App\Http\Controllers\CarController::class, 'store']);
    Route::post('api/car/update', [App\Http\Controllers\CarController::class, 'update']);
    Route::delete('api/car/delete/{id}', [App\Http\Controllers\CarController::class, 'delete']);

    //LOG FEATURE
    Route::get('api/log/fetch/{row}', [App\Http\Controllers\LogController::class, 'fetchLogs']);
    Route::post('api/log/create', [App\Http\Controllers\LogController::class, 'store']);
    Route::post('api/log/update', [App\Http\Controllers\LogController::class, 'update']);
    Route::delete('api/log/delete/{id}', [App\Http\Controllers\LogController::class, 'delete']);
    Route::get('api/log/carlist', [App\Http\Controllers\LogController::class, 'getCars']);
    Route::post('api/log/filtered', [App\Http\Controllers\LogController::class, 'fetch_filtered']);

    //USER FEATURE
    Route::get('api/user/fetch/{row}', [App\Http\Controllers\UserController::class, 'fetchUsers']);
    Route::post('api/user/create', [App\Http\Controllers\UserController::class, 'store']);
    Route::post('api/user/update', [App\Http\Controllers\UserController::class, 'update']);
    Route::delete('api/user/delete/{id}', [App\Http\Controllers\UserController::class, 'delete']);
});

Route::group(['middleware' => ['role:USER']], function () {
    Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'userDashboard'])->name('user.dashboard');

});
