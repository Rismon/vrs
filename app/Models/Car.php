<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;
    protected $fillable = ['name','plate_no'];

    public function logs()
    {
        return $this->morphMany(Log::class, 'logable');
    }
}
