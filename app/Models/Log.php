<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    use HasFactory;
    protected $fillable = ['name','car_id','log_no','depart_dt','return_dt','purpose',
    'depart_milleage','return_milleage','total_milleage','driver','destination','remark','resit_no','liter'
    ,'claim'];

    public function logable()
    {
        return $this->morphTo();
    }
    public function car()
    {
        return $this->belongsTo(Car::class, 'car_id', 'id');
    }
}
