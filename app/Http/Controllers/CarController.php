<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Car;
use App\Http\Resources\ItemResource;
use Exception;

class CarController extends Controller
{
    public function index(){
        return view('admin.car.index');
    }
    public function fetchCars($row){

        $cars = Car::paginate($row);
        return ItemResource::collection($cars);
    }
    public function store(Request $request){

        $data = $request->car;
        if(!empty($data)){
            Car::create([
                'name' => $data['name'],
                'plate_no' => $data['plate_no']
            ]);
        }
        $cars = Car::all();

        return ItemResource::collection($cars);
    }
    public function update(Request $request){

        $data = $request->car;
        if(!empty($data)){
            $car = Car::findOrFail($data['id']);
            $car->plate_no = $data['plate_no'];
            $car->name = $data['name'];
            $car->save();
        }
        $cars = Car::all();

        return ItemResource::collection($cars);
    }
    public function delete($id){
        try
        {
            $car = Car::findOrFail($id);
            $car->delete();
            return response()->json('success');
        }
        catch (Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
}
