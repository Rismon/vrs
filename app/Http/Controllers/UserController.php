<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use app\Models\User;
use App\Http\Resources\ItemResource;
use Exception;

class UserController extends Controller
{
    public function index(){
        return view('admin.user.index');
    }
    public function getUsers($row){
        $users = User::paginate($row);
        foreach($users as $user){
            $user->role = $user->roles->pluck('name');
        }
        return ItemResource::collection($users);
    }
    public function fetchUsers($row){

        $users = User::paginate($row);
        foreach($users as $user){
            $user->role = $user->roles->pluck('name');
        }
        return $this->getUsers($row);
    }
    public function store(Request $request){

        $data = $request->user;

        if(!empty($data)){
            $user = User::create([
                'name' => $data['name'],
                'staff_id' => $data['staff_id'],
                'email' => $data['email'],
                'password' => bcrypt ($data['password']),
                'status' => $data['status'],
            ]);

            $user->assignRole($data['role']);
        }
        return $this->getUsers(10);
    }
    public function update(Request $request){

        $data = $request->car;
        if(!empty($data)){
            $car = User::findOrFail($data['id']);
            $car->plate_no = $data['plate_no'];
            $car->name = $data['name'];
            $car->save();
        }
        return $this->getUsers(10);
    }
    public function delete($id){
        try
        {
            $car = User::findOrFail($id);
            $car->delete();
            return response()->json('success');
        }
        catch (Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
}
