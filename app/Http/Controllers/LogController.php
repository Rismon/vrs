<?php

namespace App\Http\Controllers;

use App\Http\Resources\ItemResource;
use App\Models\Car;
use App\Models\Log;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class LogController extends Controller
{
    public function index(){
        return view('admin.log.index');
    }
    public function fetchLogs($row){

        $logs = DB::table('logs')->join('cars', 'logs.car_id', '=', 'cars.id')
                ->select('logs.*', 'cars.plate_no','cars.name')->orderBy('logs.created_at','DESC')->paginate($row);

        return $logs;
    }
    public function getCars(){
        $cars = Car::all();
        return ItemResource::collection($cars);
    }
    public function store(Request $request){

        //create running no
        $running_no = 1000000;
        $logId = Log::select('id')->orderBy('id', 'desc')->first();
        if(!empty($logId))$running_no = $running_no + $logId->id;
        $newNo = 'Log/'.$running_no;

        $data = $request->log;
        $row = $request->row;
        if(!empty($data)){
            $car = $data['car'];
            Log::create([
                'car_id' => $car['id'],
                'log_no' => $newNo,
                'depart_dt' =>$data['depart_dt'],
                'return_dt' => $data['return_dt'],
                'purpose' => $data['purpose'],
                'driver' => $data['driver'],
                'depart_milleage' => $data['depart_milleage'],
                'return_milleage' => $data['return_milleage'],
                'total_milleage' => $data['total_milleage'],
                'destination' => $data['destination'],
                'remark' => $data['remark'],
                'resit_no' => $data['resit_no'],
                'liter' => $data['liter'],
                'claim' => $data['claim']


            ]);
        }
        $logs = DB::table('logs')->join('cars', 'logs.car_id', '=', 'cars.id')
                ->select('logs.*', 'cars.*')->orderBy('logs.created_at','DESC')->paginate($row);

        return $logs;
    }
    public function update(Request $request){

        $data = $request->log;
        $row = $request->row;
        if(!empty($data)){
            $log = Log::findOrFail($data['id']);
            $car = $data['car'];
            $log->car_id = $car['id'];
            $log->depart_dt = $data['depart_dt'];
            $log->return_dt = $data['return_dt'];
            $log->purpose = $data['purpose'];
            $log->driver = $data['driver'];
            $log->depart_milleage = $data['depart_milleage'];
            $log->return_milleage = $data['return_milleage'];
            $log->total_milleage = $data['total_milleage'];
            $log->destination = $data['destination'];
            $log->remark = $data['remark'];
            $log->resit_no = $data['resit_no'];
            $log->liter = $data['liter'];
            $log->liter = $data['claim'];
            $log->save();
        }
        $logs = Log::orderBy('created_at')->paginate($row);
        foreach($logs as $log){
            $car = Car::find($log->car_id);
            $log->car = $car;
        }
        return $logs;
    }
    public function delete($id){
        try
        {
            $log = Log::findOrFail($id);
            $log->delete();
            return response()->json('success');
        }
        catch (Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
    public function fetch_filtered(Request $request){

        $req = $request->all();
        $data = [];
        $fType = $req['fType'];
        $row = $req['row'];

        $query = DB::table('logs')->join('cars', 'logs.car_id', '=', 'cars.id')
                ->select('logs.*', 'cars.*');

        foreach ($req['data'] as $value) {

            $data2['field'] = $value['form1'];
            $data2['contain'] = $value['form2'];
            $data[] = $data2;
        }
        $length = count($data);

        if($fType== 'where'){
            foreach ($data as $value) {

                if($value['field']=='month'){
                    $query = $query->whereMonth('depart_dt','=',$value['contain']);
                }else{
                    $query = $query->where($value['field'],'like','%'.$value['contain'].'%');
                }

            }
        }else{

            for ( $x = 0 ; $x < $length ; $x ++ )
            {
                if ( $x == 0 )
                {
                    if($data[$x]['field']=='month'){
                        $query = $query->whereMonth('depart_dt','=',$data[$x]['contain']);
                    }else{
                        $query = $query->where($data[$x]['field'],'like','%'.$data[$x]['contain'].'%');
                    }

                }else {
                    if($data[$x]['field']=='month'){
                        $query = $query->orWhereMonth('depart_dt','=',$data[$x]['contain']);
                    }else{
                        $query = $query->orwhere($data[$x]['field'],'like','%'.$data[$x]['contain'].'%');
                    }

                }


            }
        }

        $logs = $query->orderBy('logs.created_at')->paginate($row);
        return  $logs;
        //return view('admin.asset.asset_add');
        //return $request;
    }

}
