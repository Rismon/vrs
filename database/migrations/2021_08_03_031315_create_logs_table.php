<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->id();
            $table->string('car_id');
            $table->string('log_no')->unique();
            $table->dateTime('depart_dt', $precision = 0);
            $table->dateTime('return_dt', $precision = 0);
            $table->string('purpose');
            $table->decimal('depart_milleage', $precision = 8, $scale = 1);
            $table->decimal('return_milleage', $precision = 8, $scale = 1);
            $table->decimal('total_milleage', $precision = 8, $scale = 1);
            $table->string('driver');
            $table->string('destination');
            $table->string('remark')->nullable();
            $table->string('resit_no')->nullable();
            $table->string('liter')->nullable();
            $table->decimal('claim', $precision = 8, $scale = 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
