<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name'=>'su',
            'email'=>'rismon.hq.aws@gmail.com',
            'staff_id'=>'AWS000',
            'password'=>bcrypt('miau1993')
        ]);
        $admin->assignRole('SUADMIN');
        $admin = User::create([
            'name'=>'Engcomboy',
            'email'=>'rismonjohn@gmail.com',
            'staff_id'=>'AWS002',
            'password'=>bcrypt('miau1993')
        ]);
        $admin->assignRole('ADMIN');

    }
}
