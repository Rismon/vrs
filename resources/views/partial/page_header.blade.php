    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">
                @if(View::hasSection('headerTitle'))
                    @yield('headerTitle')
                @else
                    Content
                @endif
              </h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              @if (View::hasSection('breadCrumb'))
                @yield('breadCrumb')
              @else
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                </ol>
              @endif

            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->
