<!-- Main Sidebar Container -->
<aside class="main-sidebar elevation-4 sidebar-light-primary">
    <!-- Brand Logo -->
    <a href="../../index3.html" class="brand-link">
        <div class="text-center">
            <img src="{{asset('images/logo.png')}}" alt="User profile picture" style="height: 50px;padding:0px;margin:0px;">
        </div>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
            @role('SUADMIN')
            <li class="nav-item">
                <a href="{{route('admin.dashboard')}}" class="nav-link {{ (strpos(Route::currentRouteName(), 'dashboard') !== false) ? 'active' : '' }}">
                    <i class="nav-icon fas fa-home"></i>
                    <p>
                        Dashboard
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('admin.car')}}" class="nav-link {{ (strpos(Route::currentRouteName(), 'admin.car') !== false) ? 'active' : '' }}">
                    <i class="nav-icon fas fa-car"></i>
                    <p>
                        Cars
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('admin.log')}}" class="nav-link {{ (strpos(Route::currentRouteName(), 'admin.log') !== false) ? 'active' : '' }}">
                    <i class="nav-icon fas fa-clipboard"></i>
                    <p>
                        Log
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('admin.user')}}" class="nav-link {{ (strpos(Route::currentRouteName(), 'admin.user') !== false) ? 'active' : '' }}">
                    <i class="nav-icon fas fa-male"></i>
                    <p>
                        User
                    </p>
                </a>
            </li>
            @endrole
            @role('ADMIN')
            <li class="nav-item">
                <a href="{{route('admin.dashboard')}}" class="nav-link {{ (strpos(Route::currentRouteName(), 'dashboard') !== false) ? 'active' : '' }}">
                    <i class="nav-icon fas fa-home"></i>
                    <p>
                        Dashboard
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('admin.hr-admin-tool')}}" class="nav-link {{ (strpos(Route::currentRouteName(), 'admin.hr-admin-tool') !== false) ? 'active' : '' }}">
                    <i class="nav-icon fas fa-cogs"></i>
                    <p>
                        HR Admin Tools
                    </p>
                </a>
            </li>

           @else
                @role('USER')
                    <li class="nav-item">
                        <a href="{{route('user.dashboard')}}" class="nav-link {{ (strpos(Route::currentRouteName(), 'dashboard') !== false) ? 'active' : '' }}">
                            <i class="nav-icon fas fa-home"></i>
                            <p>
                                Dashboard
                            </p>
                        </a>
                    </li>
                @else

                @endrole
           @endrole


        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
  @push('styles')
  <style>
    .logo {
    width: 120px;
    height: 50px;
    text-align:center;
    }

    .logo img {
    display:inline-block;
    }
    </style>
  @endpush
