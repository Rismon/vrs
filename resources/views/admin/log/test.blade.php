@extends('default')
@section('headerTitle','Log')
@push('styles')
    <link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
@endpush
@section('content')
<div class="col-sm-6">
    <div class="form-group">
        <label>Return Date/Time:</label>
        <div class="input-group date" id="returndt" data-target-input="nearest">
            <input type="text" class="form-control form-control-sm datetimepicker-input" data-target="#returndt">
            <div class="input-group-append" data-target="#returndt" data-toggle="datetimepicker">
                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
            </div>
        </div>
    </div>
</div>


@endsection

@push('scripts')
    <script src="/js/app.js"></script>
    <script src="{{asset('plugins/moment/moment.min.js')}}"></script>
    <script src="{{asset('plugins/inputmask/jquery.inputmask.min.js')}}"></script>
    <script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
    <script>
        $(function () {


          //Datemask dd/mm/yyyy
          $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
          //Datemask2 mm/dd/yyyy
          $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
          //Money Euro
          $('[data-mask]').inputmask()

          //Date and time picker
          $('#depart_dt').datetimepicker({ icons: { time: 'far fa-clock' } });
          $('#returndt').datetimepicker({ icons: { time: 'far fa-clock' } });

          $('#reservationdatetime').datetimepicker({ icons: { time: 'far fa-clock' } });

        })

      </script>
@endpush
