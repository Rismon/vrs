@extends('default')
@section('headerTitle','Dashboard')
@section('content')
<div class="row">
    <div class="col-12 col-sm-6 col-md-3">
      <div class="info-box">
        <span class="info-box-icon bg-info elevation-1"><i class="fas fa-building"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Projects</span>
          <span class="info-box-number">
            10
            <small>%</small>
          </span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-12 col-sm-6 col-md-3">
      <div class="info-box mb-3">
        <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-clock"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Pending OT</span>
          <span class="info-box-number">41,410</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix hidden-md-up"></div>

    <div class="col-12 col-sm-6 col-md-3">
      <div class="info-box mb-3">
        <span class="info-box-icon bg-success elevation-1"><i class="fas fa-bell"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Pending Leaves</span>
          <span class="info-box-number">760</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-12 col-sm-6 col-md-3">
      <div class="info-box mb-3">
        <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Users</span>
          <span class="info-box-number">2,000</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
  </div>
<div class="callout callout-info">
    <h5><i class="fas fa-info"></i> Announcement:</h5>
    This page has been enhanced for printing. Click the print button at the bottom of the invoice to test.
</div>
  <!-- TABLE: LATEST LEAVES -->
  <div class="card">
    <div class="card-header border-transparent">
      <h3 class="card-title">Latest Leave</h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body p-0">
      <div class="table-responsive">
        <table class="table m-0">
          <thead>
          <tr>
            <th>Staff Name</th>
            <th>Site</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
              <tr>

                  <td><a href=""></a></td>
                  <td></td>
                  <td><span class=""></span></td>
                  <td></td>

              </tr>
          </tbody>
        </table>
      </div>
      <!-- /.table-responsive -->
    </div>
    <!-- /.card-body -->
    <div class="card-footer clearfix">
      <a href="javascript:void(0)" class="btn btn-sm btn-secondary float-right">View All Leave</a>
    </div>
    <!-- /.card-footer -->
    </div>
    <!-- /.card -->
    <!-- TABLE: LATEST LEAVES -->
  <div class="card">
    <div class="card-header border-transparent">
      <h3 class="card-title">Latest Overtime</h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body p-0">
      <div class="table-responsive">
        <table class="table m-0">
          <thead>
          <tr>
            <th>Staff Name</th>
            <th>Site</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
              <tr>

                  <td><a href=""></a></td>
                  <td></td>
                  <td><span class=""></span></td>
                  <td></td>

              </tr>
          </tbody>
        </table>
      </div>
      <!-- /.table-responsive -->
    </div>
    <!-- /.card-body -->
    <div class="card-footer clearfix">
      <a href="javascript:void(0)" class="btn btn-sm btn-secondary float-right">View All Overtime</a>
    </div>
    <!-- /.card-footer -->
    </div>
    <!-- /.card -->
@endsection
